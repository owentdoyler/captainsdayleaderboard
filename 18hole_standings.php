<?php
	require_once("connect.php");
	$scores_query = "SELECT * FROM 18hole_scores WHERE score18 != 0";
	$count_query = "SELECT count(score18) as entries FROM 18hole_scores WHERE score18 != 0";
	$response = @mysqli_query($database, $scores_query);
	$countResponse = @mysqli_query($database, $count_query);
	$nameSort;
	$data = mysqli_fetch_assoc($countResponse);
	$playersEntered  = $data['entries'];
	if($response){
		$row = mysqli_fetch_array($response);

		if(count($row) > 0){
			$nameSort = false;
			require_once('18hole_query_net.php');
		}
		else{
			$nameSort = true;
			require_once('18hole_query.php');
		}
	}

	$page = $_SERVER['PHP_SELF'];
	$sec = "20";
?>
<!DOCTYPE html>
<html>
    <head>
				<meta http-equiv="refresh" content="<?php echo $sec?>;URL='<?php echo $page?>'">
        <link rel="stylesheet" type="text/css" href="style.css" />
        <title>18 Hole Standings</title>
    </head>
    <body>
        <div id="banner">
				<img src="lutt_logo_green.png" id="logo">
				<h2 id="heading">Dave Heary's Captains Prize<br>Leaderboard</h2>
        </div>

        <div id="table">
            <table id="standings">
                <tr>
                    <td><h5>POS</h5></td>
                    <td id="nameCol"><h5>PLAYER	(H'CAP)</h5></td>
					<td><h5>GROSS<br>TOTAL</h5></td>
                    <td><h5>NET<br>TOTAL</h5></td>
                </tr>
            <?php
				$score12;
				$position = 1;

				//loop throug all the players and display the standings in a table
				foreach($standings as $player){
					$topStyle = "";
					if($position <= 12  && !$nameSort && $playersEntered > 12){
						$topStyle = "style=\"background-color: #FFFF66;\"";
					}
					if($position === 12 && $player->score18Net ){
						$score12 = $player->score18Net;
					}
					if($position > 12 && @$score12 === $player->score18Net && !$nameSort && $playersEntered > 12){
						$topStyle = "style=\"background-color: #FFFF66;\"";
					}
					echo "<tr $topStyle>";
						//position
						echo "<td><p class=\"leaderboardData\">$position</p></td>";
						$position = $position + 1;

						//name
						echo "<td><p class=\"name\">$player->name <span style=\"color:#8d8d8d;\">($player->handicap)</span></p></td>";

						//18 hole gross
						$timeOrScore = $player->scoreOrTeeTime();
						echo "<td><p class=\"leaderboardData\">$timeOrScore</p></td>";


						//tee time/score
						echo "<td id=\"totalScore\"  style=\"background-color:red;\"><p class=\"leaderboardData\">";
						if($player->score18){
							echo $player->score18Net;
						}
						else{
							echo '-';
						}
						echo "</p></td>";

					echo "</tr>";
				}
			?>

            </table>
			<div class="navigation">
				<a href="index.php">PLayoff Leaderboard</a>
			</div>
    </body>
</html>
