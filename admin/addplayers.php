<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="/captainsDayLeaderboard/style.css" />
        <title>Add Players</title>
    </head>
    <body>
        <div id="banner">
				<img src="/captainsDayLeaderboard/lutt_logo_green.png" id="logo">
				<h2 id="heading">Dave Heary's Captains Prize<br>Add Players</h2>
        </div>

		<?php

					$nav =	"<div class=\"navigation\">
				<a href=\"addScores.php\" class=\"navigation\">AddScores</a>
				<br>
				<a href=\"../index.php\">Playoff Leaderboard</a>
			</div>";

			if(@$_POST['addPlayer']){
				//handle the adding of the player
				$playerName = $_POST['playername'];
				$handicap = $_POST['handicap'];
				$score18 = $_POST['score18'];
       			 $back9 = $_POST['18HoleBack9'];
				if($playerName){
					if($handicap){
						if($score18){
              				if($back9){
  								require("../connect.php");
								$netTotal = $score18 - $handicap ;
								$handicapAdjust = $handicap/2;
								$netTotal -= $handicapAdjust;
								//insert the new player and all their data
								@mysqli_query($database, "INSERT INTO playoff_scores VALUES(
									'', 
									'$playerName',
									 $handicap,
									 $handicapAdjust,
									 $score18,
									 0,0,0,0,0,0,0,0,0,
									 $netTotal,
									 $score18,
									 0,0,
									 $back9,
									 0
								)");
              				}
              				else{
  								echo "You must fill in all fields";
  							}
            			}
						else{
							echo "You must fill in all fields";
						}
					}
					else{
						echo "You must fill in all fields";
					}
				}
				else{
					echo "You must fill in all fields";
				}
			}
			else if(@$_POST['delete']){
				require("../connect.php");
				@mysqli_query($database, "DELETE FROM playoff_scores WHERE id= $_POST[delete]");
			}

		?>
		<div id="addPlayersBody">
        <form action='addplayers.php' method='post' id="addplayer">
			<table>
				<tr>
					<td>Player Name: </td>
					<td><input type='text' name='playername'/></td>
				</tr>
				<tr>
					<td>Handicap: </td>
					<td><input type='number' name='handicap'/></td>
				</tr>
				<tr>
					<td>18 Hole Score(Gross): </td>
					<td><input type='number' name='score18'/></td>
				</tr>
        		<tr>
					<td>18 Hole Back 9(Gross): </td>
					<td><input type='number' name='18HoleBack9'/></td>
				</tr>
				<tr>
					<td></td>
					<td><input type='submit' name='addPlayer' value='Add Player'/></td>
				</tr>
			</table>
		</form>

		<?php
			require_once("../query.php");
		?>
		<table id="managePlayers">
			<?php
			//loop through all the players and display them in a table
				foreach($standings as $player){
					echo "<tr>";
						echo "<td class=\"highlight\"><p id=\"playerName\">$player->name ($player->handicap) <span style=\"color:blue\">$player->score18</p></span></td>";
						echo "<td>
							<form action=\"addplayers.php\" method=\"post\" id=\"deletePlayer\">
								<input type=\"hidden\" name=\"delete\" value=\"$player->id\" />
								<input type=\"submit\" value=\"X\" style=\"color:red\"/>
							</form>
						</td>";
					echo "</tr>";
				}
			?>
		</table>


		<?php
			echo $nav;
		?>
    </body>
</html>
