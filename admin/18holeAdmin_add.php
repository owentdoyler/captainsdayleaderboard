<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="/captainsDayLeaderboard/style.css" />
        <title>Add Players(18 holes)</title>
    </head>
    <body>
        <div id="banner">
				<img src="/captainsDayLeaderboard/lutt_logo_green.png" id="logo">
				<h2 id="heading">Dave Heary's Captains Prize<br>Add Players</h2>
        </div>
		
		<?php
		
			$nav =	"<div class=\"navigation\">
				<a href=\"18holeAdmin_change.php\">Update Existing Players</a>
				<br>
				<a href=\"18holeAdmin.php\">Add 18 Hole Score</a>
			</div>";
		
			if(@$_POST['addPlayer']){
				//handle the adding of the player
				$playerName = $_POST['playername'];
				$handicap = $_POST['handicap'];
				$teeTime = $_POST['teeTime'];
				
				if($playerName){
					if($handicap){
						if($teeTime){
							require("../connect.php");
							//insert the new player and all their data 
							@mysqli_query($database, "INSERT INTO 18hole_scores VALUES(
								'', '$teeTime','$playerName', $handicap, 0, NULL
							)");
						}
						else{
							echo "You must fill in all fields";
						}
					}
					else{
						echo "You must fill in all fields";
					} 
				}
				else{
					echo "You must fill in all fields";
				}
			}
		
		?>
		<div id="addPlayersBody" style="width: 400px;">
        <form action='18holeAdmin_add.php' method='post' id="addplayer">
			<table>
				<tr>
					<td>Player Name: </td>
					<td><input type='text' name='playername'/></td>
				</tr>
				<tr>
					<td>Handicap: </td>
					<td><input type='number' name='handicap'/></td>
				</tr>
				<tr>
					<td>18 Hole Score(Gross): </td>
					<td><input type='text' name='teeTime'/></td>
				</tr>
				
				<tr>
					<td></td>
					<td><input type='submit' name='addPlayer' value='Add Player'/></td>
				</tr>
			</table>
		</form>
		
		
		<?php
			echo $nav;
		?>
    </body>
</html>