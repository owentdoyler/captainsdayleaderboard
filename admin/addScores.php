<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="/captainsDayLeaderboard/style.css" />
        <title>Update Scores</title>
    </head>
    <body>
	<div id="banner">
			<img src="/captainsDayLeaderboard/lutt_logo_green.png" id="logo">
			<h2 id="heading">Dave Heary's Captains Prize<br>Update Scores</h2>
	</div>
	<?php


		$nav =	"<div class=\"navigation\">
				<a href=\"addplayers.php\">Add Players</a>
				<br>
				<a href=\"../index.php\" class=\"navigation\">Playoff Leaderboard</a>
			</div>";
		require_once("../query.php");
		//create the table of all players
		$table = "<table id=\"playerSelectTable\">";
		foreach($standings as $player){
			$table .= "<tr class=\"highlight\">
				<td id=\"playerNameSelectTable\">$player->name ($player->handicap)</td>
				<td id=\"playerSelectButton\"><form action=\"addScores.php\" method=\"post\">
						<input type=\"hidden\" name=\"updateId\" value=\"$player->id\" />
						<input type=\"hidden\" name=\"updateName\" value=\"$player->name\" />
						<input type=\"submit\" value=\" Add Score\" />
					</form>
				</td>
			</tr>";
		}
		$table .= "</table>";

		$form = '';
			if(@$_POST['updateId'] && @$_POST['updateName'] ){
				//create and display the form for the specific player
				require("../connect.php");
				$id = $_POST['updateId'];

				$scoresQuery = "SELECT * FROM playoff_scores WHERE id=$id";
				$queryResult = @mysqli_query($database, $scoresQuery);
				$row =  mysqli_fetch_array($queryResult);
				$scores = array(
					$row['score1'] != NULL? $row['score1']:'-',
					$row['score2'] != NULL? $row['score2']:'-',
					$row['score3'] != NULL? $row['score3']:'-',
					$row['score4'] != NULL? $row['score4']:'-',
					$row['score5'] != NULL? $row['score5']:'-',
					$row['score6'] != NULL? $row['score6']:'-',
					$row['score7'] != NULL? $row['score7']:'-',
					$row['score8'] != NULL? $row['score8']:'-',
					$row['score9'] != NULL? $row['score9']:'-'
				);

				$form="<form action=\"addScores.php\" method=\"post\" id=\"scoreUpdateForm\">
					<input type=\"hidden\" name=\"scoreUpdateId\" value=\"$id\"/>
					<table id=\"scoreInputTable\">
						<tr id=\"scoreUpdateNameHeading\"><td colspan=\"9\" class=\"highlight\">$_POST[updateName]</td></tr>
						<tr class=\"highlight\">
							<td id=\"holeSelector\" colspan=\"4\">
								Hole<br>
								<select name=\"holeSelect\" form=\"scoreUpdateForm\">
									<option value=\"score1\">1</option>
									<option value=\"score2\">2</option>
									<option value=\"score3\">3</option>
									<option value=\"score4\">4</option>
									<option value=\"score5\">5</option>
									<option value=\"score6\">6</option>
									<option value=\"score7\">7</option>
									<option value=\"score8\">8</option>
									<option value=\"score9\">9</option>
								</select>
							</td>
							<td id=\"scoreEntry\" colspan=\"5\">
								Score(Gross) <br><input type=\"number\" name=\"holeScore\" />
							</td>
						</tr>
						<tr>
							<td class=\"highlightCenter\">Hole1<br>$scores[0]</td>
							<td class=\"highlightCenter\">Hole2<br>$scores[1]</td>
							<td class=\"highlightCenter\">Hole3<br>$scores[2]</td>
							<td class=\"highlightCenter\">Hole4<br>$scores[3]</td>
							<td class=\"highlightCenter\">Hole5<br>$scores[4]</td>
							<td class=\"highlightCenter\">Hole6<br>$scores[5]</td>
							<td class=\"highlightCenter\">Hole7<br>$scores[6]</td>
							<td class=\"highlightCenter\">Hole8<br>$scores[7]</td>
							<td class=\"highlightCenter\">Hole9<br>$scores[8]</td>
						</tr>
						<tr>
							<td colspan=\"9\">
								<input type=\"submit\" value=\"Update Score\" id=\"scoreUpdateButton\"/>
							</td>
						</tr>
					</table>
				</form>";
				echo "<div id=\"addScoreBody\">";
				echo $form;
				echo $nav;
			}
			else if(@$_POST['holeSelect'] && $_POST['holeScore'] && $_POST['scoreUpdateId']){
				//handle the update of the database when score is added
				require("../connect.php");
				$playerId = $_POST['scoreUpdateId'];
				if($response){
					$row = mysqli_fetch_array($response);
					$holeScore = $_POST['holeScore'];
					$query = "UPDATE playoff_scores SET $_POST[holeSelect]=$holeScore WHERE id=$playerId";
					@mysqli_query($database, $query);

					$player_query = "SELECT * FROM playoff_scores WHERE id=\"$playerId\"";
					$response = @mysqli_query($database, $player_query);
					if($response){
						$row = mysqli_fetch_array($response);

						$score1 = $row['score1']!=NULL? $row['score1'] : 0;
						$score2 = $row['score2']!=NULL? $row['score2'] : 0;
						$score3 = $row['score3']!=NULL? $row['score3'] : 0;
						$score4 = $row['score4']!=NULL? $row['score4'] : 0;
						$score5 = $row['score5']!=NULL? $row['score5'] : 0;
						$score6 = $row['score6']!=NULL? $row['score6'] : 0;
						$score7 = $row['score7']!=NULL? $row['score7'] : 0;
						$score8 = $row['score8']!=NULL? $row['score8'] : 0;
						$score9 = $row['score9']!=NULL? $row['score9'] : 0;

						//playoff total
						$playoffTotal =	$score1 + $score2 + $score3 + $score4 + $score5
							+ $score6 + $score7 + $score8 + $score9;

						//sum the total of the 18 holes and the extra 9
						$grossTotal = $row['score18'] + $playoffTotal;

						//net total
						$netTotal = $grossTotal - $row['handicap'] - $row['handicapAdjust'];

						//playoffTotalNet
						$playoffTotalNet = $playoffTotal - $row['handicapAdjust'];

						//countback
						$countback = ($playoffTotal + $row['18HoleBack9']) - $row['handicap'];

						$query = "UPDATE playoff_scores
							SET
						 	grossTotal=$grossTotal,
						  netTotal=$netTotal,
							playoffTotal = $playoffTotal,
							playoffTotalNett = $playoffTotalNet,
							countback =$countback
							WHERE
							 id=$playerId";
						@mysqli_query($database, $query);
					}
				}
				echo "<div id=\"addScoreBody\">";
				echo $table;
				echo $nav;
			}
			else{
				//show the table of all the players
				echo "<div id=\"addScoreBody\">";
				echo $table;
				echo $nav;
			}
	?>

    </body>

</html>
