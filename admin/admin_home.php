
<!DOCTYPE html>
<html>
    <head>
	
		<meta http-equiv="refresh" content="<?php echo $sec?>;URL='<?php echo $page?>'">
        <link rel="stylesheet" type="text/css" href="/captainsDayLeaderboard/style.css" />
        <title>Admin Home</title>
    </head>
    <body>
        <div id="banner">
				<img src="/captainsDayLeaderboard/lutt_logo_green.png" id="logo">
				<h2 id="heading">Dave Heary's's Captains Prize<br>Admin Home</h2>
        </div>
        <div id="admin">
            <table id="adminTable">
                <tr>
					<td><a href="18holeAdmin.php"><img src="/captainsDayLeaderboard/add_score.png"><br> Add 18 hole<br>Score</a></td>
					<td><a href="18holeAdmin_add.php"><img src="/captainsDayLeaderboard/add_player.png"><br> Add 18 Hole<br>Player</a></td>
					<td><a href="18holeAdmin_change.php"><img src="/captainsDayLeaderboard/update.png"><br> Update 18 Hole<br>PlayerDetails</a></td>
					<td><a href="../18hole_standings.php"><img src="/captainsDayLeaderboard/leaderboard.png"><br> 18 Hole<br>Leaderboard</a></td>
				</tr>
				<tr>
					<td><a href="addScores.php"><img src="/captainsDayLeaderboard/add_score.png"><br> Add Playoff<br>Score</a></td>
					<td><a href="addplayers.php"><img src="/captainsDayLeaderboard/add_player.png"><br>Add Playoff<br>Player</a></td>
					<td><a href="../index.php"><img src="/captainsDayLeaderboard/leaderboard.png"><br>Playoff<br>Leaderboard</a></td>
				</tr>
            </table>
		</div>
    </body>
</html>