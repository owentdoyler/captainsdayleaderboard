<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="/captainsDayLeaderboard/style.css" />
        <title>Update 18 Hole Scores</title>
    </head>
    <body>
	<div id="banner">
			<img src="/captainsDayLeaderboard/lutt_logo_green.png" id="logo">
			<h2 id="heading">Dave Heary's's Captains Prize<br>Update 18 Hole Scores</h2>
	</div>
	<?php

		$nav =	"<div class=\"navigation\">
			<a href=\"../18hole_standings.php\" class=\"navigation\">18 Hole Leaderboard</a><br>
			<a href=\"18holeAdmin_change.php\" class=\"navigation\">Change Player Details</a><br>
			<a href=\"18holeAdmin_add.php\"  class=\"navigation\">Add Player</a>
		</div>";

		require_once("../18hole_query.php");
		//create the table of all players
		$table = "<table id=\"playerSelectTable\">";
		foreach($standings as $player){
			$table .= "<tr class=\"highlight\">
				<td id=\"playerNameSelectTable\">$player->name ($player->handicap)</td>
				<td id=\"playerSelectButton\"><form action=\"18holeAdmin.php\" method=\"post\">
						<input type=\"hidden\" name=\"updateId\" value=\"$player->id\" />
						<input type=\"hidden\" name=\"updateName\" value=\"$player->name\" />
						<input type=\"submit\" value=\" Add Score\" />
					</form>
				</td>
			</tr>";
		}
		$table .= "</table>";

		$form = '';
			if(@$_POST['updateId'] && @$_POST['updateName'] ){
				//create and display the form for the specific player
				require("../connect.php");
				$id = $_POST['updateId'];

				$scoresQuery = "SELECT * FROM 18hole_scores WHERE id=$id";
				$queryResult = @mysqli_query($database, $scoresQuery);
				$row =  mysqli_fetch_array($queryResult);

				$form="<form action=\"18holeAdmin.php\" method=\"post\" id=\"scoreUpdateForm\">
					<input type=\"hidden\" name=\"scoreUpdateId\" value=\"$id\"/>
					<table id=\"scoreInputTable\">
						<tr id=\"scoreUpdateNameHeading\"><td class=\"highlight\">$_POST[updateName]</td></tr>
						<tr class=\"highlight\">
							<td id=\"scoreEntry\" style=\"padding: 10px;\">
								Score(Gross) <br><input type=\"number\" name=\"18holeScore\" />
							</td>
						</tr>
						<tr>
							<td>
								<input type=\"submit\" value=\"Update Score\" id=\"scoreUpdateButton\"/>
							</td>
						</tr>
					</table>
				</form>";
				echo "<div id=\"addScoreBody\" style=\"width: 300px;\">";
				echo $form;

				echo "</div>";
			}
			else if(@$_POST['18holeScore'] && $_POST['scoreUpdateId']){
				//handle the update of the database when score is added
				require("../connect.php");
				$playerId = $_POST['scoreUpdateId'];
				if($response){
					$row = mysqli_fetch_array($response);
					$score = $_POST['18holeScore'];
					$query = "UPDATE 18hole_scores SET score18=$score WHERE id=$playerId";
					@mysqli_query($database, $query);

					$player_query = "SELECT * FROM 18hole_scores WHERE id=\"$playerId\"";
					$response = @mysqli_query($database, $player_query);
					if($response){
						$row = mysqli_fetch_array($response);
						//sum the total of the 18 holes and the extra 6
						$netTotal = $score - $row['handicap'];
						$query = "UPDATE 18hole_scores SET score18Nett=$netTotal WHERE id=$playerId";
						@mysqli_query($database, $query);
					}

				}
				echo "<div id=\"addScoreBody\">";
				echo $table;
				echo $nav;
				echo "</div>";
			}
			else{
				//show the table of all the players
				echo "<div id=\"addScoreBody\">";
				echo $table;
				echo $nav;
				echo "</div>";
			}
	?>
    </body>

</html>
