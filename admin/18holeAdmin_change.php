<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="/captainsDayLeaderboard/style.css" />
        <title>Update 18 Hole Scores</title>
    </head>
    <body>
	<div id="banner">
			<img src="/captainsDayLeaderboard/lutt_logo_green.png" id="logo">
			<h2 id="heading">Dave Heary's Captains Prize<br>Update 18 Hole Scores</h2>
	</div>
	<?php
	
	$nav =	"<div class=\"navigation\">
				<a href=\"18holeAdmin_add.php\">Add a player</a>
				<br>
				<a href=\"18holeAdmin.php\">Add 18 Hole Score</a>
			</div>";
		
		if(@$_POST['delete']){
			require("../connect.php");
			@mysqli_query($database, "DELETE FROM 18hole_scores WHERE id= $_POST[updateId]");
		}
		else if(@$_POST['changeHandicap']){
			require("../connect.php");
			$newHandicap = $_POST['newHandicap'];
			@mysqli_query($database, "UPDATE 18hole_scores SET handicap = $newHandicap WHERE id= $_POST[updateId]");
		}
	
		require_once("../18hole_query.php");
		//create the table of all players
		$table = "<table id=\"playerSelectTable\">";
		foreach($standings as $player){
			$table .= "<tr class=\"highlight\">
				<td id=\"playerNameSelectTable\">$player->name ($player->handicap)</td>
				<td id=\"playerSelectButton\"><form action=\"18holeAdmin_change.php\" method=\"post\">
						<input type=\"hidden\" name=\"updateId\" value=\"$player->id\" />
						<input type=\"hidden\" name=\"updateName\" value=\"$player->name\" />
						<input type=\"submit\" value=\"Delete Player\" name=\"delete\"/>
						<input type=\"number\" name=\"newHandicap\"/>
						<input type=\"submit\" value=\"Update Handicap\" name=\"changeHandicap\" label=\"New Handicap\"/>
					</form>
				</td>
			</tr>";
		}
		$table .= "</table>";

				//show the table of all the players
				echo "<div id=\"addScoreBody\">";
				echo $table;
				echo $nav;
		
	?>	
    </body>
	
</html>