-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 14, 2017 at 12:54 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scores`
--

-- --------------------------------------------------------

--
-- Table structure for table `18hole_scores`
--

CREATE TABLE `18hole_scores` (
  `id` int(11) NOT NULL,
  `teeTime` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `handicap` int(11) NOT NULL,
  `score18` int(11) DEFAULT NULL,
  `score18Nett` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `18hole_scores`
--

INSERT INTO `18hole_scores` (`id`, `teeTime`, `name`, `handicap`, `score18`, `score18Nett`) VALUES
(1, '07:00:00.00000', 'Michael Feeney', 9, 95, 86),
(2, '07:00:00.00000', 'David Riley', 7, 98, 91),
(3, '07:00:00.00000', 'Kevin Halpin', 12, 89, 77),
(4, '07:00:00.00000', 'Gregory Harris', 26, 0, NULL),
(5, '07:30:00.00000', 'Michael Farrell', 18, 117, 99),
(6, '07:30:00.00000', 'Noel Heffernan', 13, 96, 83),
(7, '07:30:00.00000', 'Peter Keogh', 17, 94, 77),
(8, '07:30:00.00000', 'John Lawlor', 7, 87, 80),
(9, '07:40:00.00000', 'Patrick Fitzgerald', 18, 98, 80),
(10, '08:00:00.00000', 'Marc Guerin', 7, 85, 78),
(11, '08:00:00.00000', 'Robert Emmett', 13, 90, 77),
(12, '08:00:00.00000', 'Brian Smithers', 15, 100, 85),
(13, '08:00:00.00000', 'Maurice McNamara', 17, 0, NULL),
(14, '08:10:00.00000', 'Ronan Rooney', 16, 91, 75),
(15, '08:10:00.00000', 'Joseph O\'Connor', 13, 91, 78),
(16, '08:10:00.00000', 'JW McCrory', 22, 96, 74),
(17, '08:20:00.00000', 'Marty Crawford', 18, 92, 74),
(18, '08:20:00.00000', 'Padraic Brennan', 18, 100, 82),
(19, '08:20:00.00000', 'Frank Mee', 22, 0, NULL),
(21, '08:30:00.00000', 'Michael Quinlan', 19, 0, NULL),
(22, '08:30:00.00000', 'Ray Sinnott', 16, 0, NULL),
(25, '08:40:00.00000', 'Enda Faughnan', 27, 110, 83),
(26, '08:40:00.00000', 'Jerry O\'Mahony', 19, 0, NULL),
(27, '08:40:00.00000', 'Fred Hickey', 21, 0, NULL),
(28, '08:40:00.00000', 'Paul Tobin', 10, 95, 85),
(29, '08:50:00.00000', 'Joe Lynch', 13, 92, 79),
(30, '08:50:00.00000', 'David Heary', 8, 94, 86),
(31, '08:50:00.00000', 'Eugene Daly', 11, 91, 80),
(32, '08:50:00.00000', 'Mark McClean', 12, 0, NULL),
(33, '09:00:00.00000', 'Gerard Farrelly', 6, 88, 82),
(34, '09:00:00.00000', 'Oliver Coughlan', 12, 96, 84),
(35, '09:00:00.00000', 'Brian Bellew', 18, 98, 80),
(36, '09:00:00.00000', 'Martin Moore', 17, 0, NULL),
(37, '09:10', 'Colm Kelly', 6, 83, 77),
(38, '09:10', 'Alan O\'Connor', 20, 105, 85),
(39, '09:10', 'David Bergin', 16, 99, 83),
(41, '09:20', 'Alan Bergin', 12, 102, 90),
(42, '09:20', 'Dave Bergin Snr', 28, 0, NULL),
(43, '09:20', 'Dermot Dooley', 25, 106, 81),
(44, '09:20', 'Edward Byrne', 24, 0, NULL),
(45, '09:30', 'Eamonn Coghlan', 13, 92, 79),
(46, '09:30', 'Ivan Cosgrave', 20, 98, 78),
(47, '09:30', 'John Smyth', 16, 94, 78),
(48, '09:30:00.00000', 'Michael Quinlan', 17, 0, NULL),
(50, '09:50', 'David Curley', 25, 99, 74),
(51, '09:50', 'Michael Fetherston', 9, 80, 71),
(52, '09:50', 'MJ Fetherston', 23, 93, 70),
(54, '10:20', 'Jerry Kelleher', 27, 0, NULL),
(56, '10:20', 'Petr Pandula', 19, 0, NULL),
(57, '10:40', 'Niall Carty', 10, 98, 88),
(58, '10:40', 'David Hague', 11, 86, 75),
(59, '10:40', 'Rory Kilbane', 10, 89, 79),
(60, '10:40', 'Aidan McCormack', 16, 99, 83),
(61, '10:50', 'Jonathan Sheahan', 17, 0, NULL),
(62, '10:50', 'Alan Devlin', 25, 0, NULL),
(63, '10:50', 'Simon Madden', 16, 96, 80),
(65, '11:00', 'Bobby Hawkshaw', 6, 0, NULL),
(66, '11:00:', 'Des McCormack', 16, 0, NULL),
(67, '11:00', 'Tadhg Gunnell', 18, 0, NULL),
(68, '11:10', 'Martin Doyle', 9, 86, 77),
(69, '11:10', 'Jonathan Doyle', 10, 84, 74),
(70, '11:10', 'Owen Doyle', 7, 89, 82),
(71, '11:10', 'Kieran Fitzmaurice', 10, 93, 83),
(143, '09:10:00.00000', 'Liam Lynch', 7, 87, 80),
(214, '94', 'Shane Maguire', 9, 94, 85),
(215, '99', 'Anthony Smiddy', 19, 99, 80),
(216, '110', 'Brian Otridge', 24, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `playoff_scores`
--

CREATE TABLE `playoff_scores` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `handicap` int(11) NOT NULL,
  `handicapAdjust` double NOT NULL,
  `score18` int(11) NOT NULL,
  `score1` int(11) DEFAULT NULL,
  `score2` int(11) DEFAULT NULL,
  `score3` int(11) DEFAULT NULL,
  `score4` int(11) DEFAULT NULL,
  `score5` int(11) DEFAULT NULL,
  `score6` int(11) DEFAULT NULL,
  `score7` int(11) NOT NULL,
  `score8` int(11) NOT NULL,
  `score9` int(11) NOT NULL,
  `netTotal` double NOT NULL,
  `grossTotal` int(11) NOT NULL,
  `playoffTotal` int(11) NOT NULL,
  `playoffTotalNett` double NOT NULL,
  `18HoleBack9` int(11) NOT NULL,
  `countback` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `playoff_scores`
--

INSERT INTO `playoff_scores` (`id`, `name`, `handicap`, `handicapAdjust`, `score18`, `score1`, `score2`, `score3`, `score4`, `score5`, `score6`, `score7`, `score8`, `score9`, `netTotal`, `grossTotal`, `playoffTotal`, `playoffTotalNett`, `18HoleBack9`, `countback`) VALUES
(39, 'MJ Fetherston', 23, 8, 93, 5, 7, 4, 4, 5, 5, 8, 5, 0, 105, 136, 43, 35, 0, 113),
(40, 'Michael Fetherston', 9, 3, 80, 3, 6, 4, 5, 4, 5, 5, 0, 0, 100, 112, 32, 29, 0, 103),
(41, 'JW McCrory', 22, 7, 96, 6, 8, 5, 6, 7, 6, 5, 0, 0, 110, 139, 43, 36, 0, 117),
(42, 'Marty Crawford', 18, 6, 92, 9, 6, 5, 4, 5, 4, 0, 0, 0, 101, 125, 0, 0, 0, 0),
(43, 'David Curley', 25, 8, 99, 7, 7, 6, 5, 6, 5, 0, 0, 0, 102, 135, 0, 0, 0, 0),
(44, 'Jonathan Doyle', 10, 3, 84, 4, 5, 6, 3, 6, 6, 5, 5, 6, 117, 130, 0, 0, 0, 0),
(45, 'Ronan Rooney', 16, 5, 91, 5, 7, 6, 7, 5, 5, 0, 0, 0, 105, 126, 0, 0, 0, 0),
(46, 'David Hague', 11, 4, 86, 5, 6, 6, 4, 7, 4, 0, 0, 0, 103, 118, 0, 0, 0, 0),
(47, 'Kevin Halpin', 12, 4, 89, 5, 5, 5, 3, 5, 5, 0, 0, 0, 101, 117, 0, 0, 0, 0),
(48, 'Peter Keogh', 17, 6, 94, 5, 4, 5, 4, 5, 5, 0, 0, 0, 99, 122, 0, 0, 0, 0),
(49, 'Robert Emmett', 13, 4, 90, 5, 5, 5, 3, 5, 4, 5, 0, 0, 105, 122, 32, 28, 0, 109),
(50, 'Colm Kelly', 6, 2, 83, 4, 5, 6, 3, 5, 6, 0, 0, 0, 104, 112, 0, 0, 0, 0),
(51, 'Martin Doyle', 9, 3, 86, 5, 5, 5, 3, 5, 4, 0, 0, 0, 101, 113, 0, 0, 0, 0),
(52, 'Owen Doyle', 8, 4, 80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 68, 80, 0, 0, 40, 0),
(53, 'Tiger Woods', 7, 3.5, 80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69.5, 80, 0, 0, 40, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `18hole_scores`
--
ALTER TABLE `18hole_scores`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `playoff_scores`
--
ALTER TABLE `playoff_scores`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `18hole_scores`
--
ALTER TABLE `18hole_scores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=217;
--
-- AUTO_INCREMENT for table `playoff_scores`
--
ALTER TABLE `playoff_scores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
