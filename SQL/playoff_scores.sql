-- phpMyAdmin SQL Dump
-- version 2.11.11.3
-- http://www.phpmyadmin.net
--
-- Host: mysql2.mylogin.ie
-- Generation Time: Jul 10, 2017 at 09:56 PM
-- Server version: 5.5.53
-- PHP Version: 5.6.30

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `luttrel`
--

-- --------------------------------------------------------

--
-- Table structure for table `playoff_scores`
--

CREATE TABLE IF NOT EXISTS `playoff_scores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `handicap` int(11) NOT NULL,
  `handicapAdjust` int(11) NOT NULL,
  `score18` int(11) NOT NULL,
  `score1` int(11) DEFAULT NULL,
  `score2` int(11) DEFAULT NULL,
  `score3` int(11) DEFAULT NULL,
  `score4` int(11) DEFAULT NULL,
  `score5` int(11) DEFAULT NULL,
  `score6` int(11) DEFAULT NULL,
  `netTotal` int(11) NOT NULL,
  `grossTotal` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=52 ;

--
-- Dumping data for table `playoff_scores`
--

INSERT INTO `playoff_scores` (`id`, `name`, `handicap`, `handicapAdjust`, `score18`, `score1`, `score2`, `score3`, `score4`, `score5`, `score6`, `netTotal`, `grossTotal`) VALUES
(39, 'MJ Fetherston', 23, 8, 93, 5, 7, 4, 4, 5, 5, 92, 123),
(40, 'Michael Fetherston', 9, 3, 80, 5, 6, 4, 5, 4, 5, 97, 109),
(41, 'JW McCrory', 22, 7, 96, 6, 8, 5, 6, 7, 6, 105, 134),
(42, 'Marty Crawford', 18, 6, 92, 9, 6, 5, 4, 5, 4, 101, 125),
(43, 'David Curley', 25, 8, 99, 7, 7, 6, 5, 6, 5, 102, 135),
(44, 'Jonathan Doyle', 10, 3, 84, 5, 5, 6, 3, 6, 6, 102, 115),
(45, 'Ronan Rooney', 16, 5, 91, 5, 7, 6, 7, 5, 5, 105, 126),
(46, 'David Hague', 11, 4, 86, 5, 6, 6, 4, 7, 4, 103, 118),
(47, 'Kevin Halpin', 12, 4, 89, 5, 5, 5, 3, 5, 5, 101, 117),
(48, 'Peter Keogh', 17, 6, 94, 5, 4, 5, 4, 5, 5, 99, 122),
(49, 'Robert Emmett', 13, 4, 90, 5, 5, 5, 3, 5, 4, 100, 117),
(50, 'Colm Kelly', 6, 2, 83, 4, 5, 6, 3, 5, 6, 104, 112),
(51, 'Martin Doyle', 9, 3, 86, 5, 5, 5, 3, 5, 4, 101, 113);
