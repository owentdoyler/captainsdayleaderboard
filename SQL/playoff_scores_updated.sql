-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 11, 2017 at 10:03 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scores`
--

-- --------------------------------------------------------

--
-- Table structure for table `playoff_scores`
--

CREATE TABLE `playoff_scores` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `handicap` int(11) NOT NULL,
  `handicapAdjust` int(11) NOT NULL,
  `score18` int(11) NOT NULL,
  `score1` int(11) DEFAULT NULL,
  `score2` int(11) DEFAULT NULL,
  `score3` int(11) DEFAULT NULL,
  `score4` int(11) DEFAULT NULL,
  `score5` int(11) DEFAULT NULL,
  `score6` int(11) DEFAULT NULL,
  `score7` int(11) NOT NULL,
  `score8` int(11) NOT NULL,
  `score9` int(11) NOT NULL,
  `netTotal` int(11) NOT NULL,
  `grossTotal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `playoff_scores`
--

INSERT INTO `playoff_scores` (`id`, `name`, `handicap`, `handicapAdjust`, `score18`, `score1`, `score2`, `score3`, `score4`, `score5`, `score6`, `score7`, `score8`, `score9`, `netTotal`, `grossTotal`) VALUES
(39, 'MJ Fetherston', 23, 8, 93, 5, 7, 4, 4, 5, 5, 8, 0, 0, 100, 131),
(40, 'Michael Fetherston', 9, 3, 80, 5, 6, 4, 5, 4, 5, 0, 0, 0, 97, 109),
(41, 'JW McCrory', 22, 7, 96, 6, 8, 5, 6, 7, 6, 0, 0, 0, 105, 134),
(42, 'Marty Crawford', 18, 6, 92, 9, 6, 5, 4, 5, 4, 0, 0, 0, 101, 125),
(43, 'David Curley', 25, 8, 99, 7, 7, 6, 5, 6, 5, 0, 0, 0, 102, 135),
(44, 'Jonathan Doyle', 10, 3, 84, 4, 5, 6, 3, 6, 6, 5, 5, 0, 111, 124),
(45, 'Ronan Rooney', 16, 5, 91, 5, 7, 6, 7, 5, 5, 0, 0, 0, 105, 126),
(46, 'David Hague', 11, 4, 86, 5, 6, 6, 4, 7, 4, 0, 0, 0, 103, 118),
(47, 'Kevin Halpin', 12, 4, 89, 5, 5, 5, 3, 5, 5, 0, 0, 0, 101, 117),
(48, 'Peter Keogh', 17, 6, 94, 5, 4, 5, 4, 5, 5, 0, 0, 0, 99, 122),
(49, 'Robert Emmett', 13, 4, 90, 5, 5, 5, 3, 5, 4, 0, 0, 0, 100, 117),
(50, 'Colm Kelly', 6, 2, 83, 4, 5, 6, 3, 5, 6, 0, 0, 0, 104, 112),
(51, 'Martin Doyle', 9, 3, 86, 5, 5, 5, 3, 5, 4, 0, 0, 0, 101, 113);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `playoff_scores`
--
ALTER TABLE `playoff_scores`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `playoff_scores`
--
ALTER TABLE `playoff_scores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
