
 
-- phpMyAdmin SQL Dump
-- version 2.11.11.3
-- http://www.phpmyadmin.net
--
-- Host: mysql2.mylogin.ie
-- Generation Time: Jul 10, 2017 at 09:54 PM
-- Server version: 5.5.53
-- PHP Version: 5.6.30

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `luttrel`
--

-- --------------------------------------------------------

--
-- Table structure for table `18hole_scores`
--

CREATE TABLE IF NOT EXISTS `18hole_scores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `teeTime` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `handicap` int(11) NOT NULL,
  `score18` int(11) DEFAULT NULL,
  `score18Nett` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=217 ;

--
-- Dumping data for table `18hole_scores`
--

INSERT INTO `18hole_scores` (`id`, `teeTime`, `name`, `handicap`, `score18`, `score18Nett`) VALUES
(1, '07:00:00.00000', 'Michael Feeney', 9, 95, 86),
(2, '07:00:00.00000', 'David Riley', 7, 98, 91),
(3, '07:00:00.00000', 'Kevin Halpin', 12, 89, 77),
(4, '07:00:00.00000', 'Gregory Harris', 26, 0, NULL),
(5, '07:30:00.00000', 'Michael Farrell', 18, 117, 99),
(6, '07:30:00.00000', 'Noel Heffernan', 13, 96, 83),
(7, '07:30:00.00000', 'Peter Keogh', 17, 94, 77),
(8, '07:30:00.00000', 'John Lawlor', 7, 87, 80),
(9, '07:40:00.00000', 'Patrick Fitzgerald', 18, 98, 80),
(10, '08:00:00.00000', 'Marc Guerin', 7, 85, 78),
(11, '08:00:00.00000', 'Robert Emmett', 13, 90, 77),
(12, '08:00:00.00000', 'Brian Smithers', 15, 100, 85),
(13, '08:00:00.00000', 'Maurice McNamara', 17, 0, NULL),
(14, '08:10:00.00000', 'Ronan Rooney', 16, 91, 75),
(15, '08:10:00.00000', 'Joseph O''Connor', 13, 91, 78),
(16, '08:10:00.00000', 'JW McCrory', 22, 96, 74),
(17, '08:20:00.00000', 'Marty Crawford', 18, 92, 74),
(18, '08:20:00.00000', 'Padraic Brennan', 18, 100, 82),
(19, '08:20:00.00000', 'Frank Mee', 22, 0, NULL),
(21, '08:30:00.00000', 'Michael Quinlan', 19, 0, NULL),
(22, '08:30:00.00000', 'Ray Sinnott', 16, 0, NULL),
(25, '08:40:00.00000', 'Enda Faughnan', 27, 110, 83),
(26, '08:40:00.00000', 'Jerry O''Mahony', 19, 0, NULL),
(27, '08:40:00.00000', 'Fred Hickey', 21, 0, NULL),
(28, '08:40:00.00000', 'Paul Tobin', 10, 95, 85),
(29, '08:50:00.00000', 'Joe Lynch', 13, 92, 79),
(30, '08:50:00.00000', 'David Heary', 8, 94, 86),
(31, '08:50:00.00000', 'Eugene Daly', 11, 91, 80),
(32, '08:50:00.00000', 'Mark McClean', 12, 0, NULL),
(33, '09:00:00.00000', 'Gerard Farrelly', 6, 88, 82),
(34, '09:00:00.00000', 'Oliver Coughlan', 12, 96, 84),
(35, '09:00:00.00000', 'Brian Bellew', 18, 98, 80),
(36, '09:00:00.00000', 'Martin Moore', 17, 0, NULL),
(37, '09:10', 'Colm Kelly', 6, 83, 77),
(38, '09:10', 'Alan O''Connor', 20, 105, 85),
(39, '09:10', 'David Bergin', 16, 99, 83),
(41, '09:20', 'Alan Bergin', 12, 102, 90),
(42, '09:20', 'Dave Bergin Snr', 28, 0, NULL),
(43, '09:20', 'Dermot Dooley', 25, 106, 81),
(44, '09:20', 'Edward Byrne', 24, 0, NULL),
(45, '09:30', 'Eamonn Coghlan', 13, 92, 79),
(46, '09:30', 'Ivan Cosgrave', 20, 98, 78),
(47, '09:30', 'John Smyth', 16, 94, 78),
(48, '09:30:00.00000', 'Michael Quinlan', 17, 0, NULL),
(50, '09:50', 'David Curley', 25, 99, 74),
(51, '09:50', 'Michael Fetherston', 9, 80, 71),
(52, '09:50', 'MJ Fetherston', 23, 93, 70),
(54, '10:20', 'Jerry Kelleher', 27, 0, NULL),
(56, '10:20', 'Petr Pandula', 19, 0, NULL),
(57, '10:40', 'Niall Carty', 10, 98, 88),
(58, '10:40', 'David Hague', 11, 86, 75),
(59, '10:40', 'Rory Kilbane', 10, 89, 79),
(60, '10:40', 'Aidan McCormack', 16, 99, 83),
(61, '10:50', 'Jonathan Sheahan', 17, 0, NULL),
(62, '10:50', 'Alan Devlin', 25, 0, NULL),
(63, '10:50', 'Simon Madden', 16, 96, 80),
(65, '11:00', 'Bobby Hawkshaw', 6, 0, NULL),
(66, '11:00:', 'Des McCormack', 16, 0, NULL),
(67, '11:00', 'Tadhg Gunnell', 18, 0, NULL),
(68, '11:10', 'Martin Doyle', 9, 86, 77),
(69, '11:10', 'Jonathan Doyle', 10, 84, 74),
(70, '11:10', 'Owen Doyle', 7, 89, 82),
(71, '11:10', 'Kieran Fitzmaurice', 10, 93, 83),
(143, '09:10:00.00000', 'Liam Lynch', 7, 87, 80),
(214, '94', 'Shane Maguire', 9, 94, 85),
(215, '99', 'Anthony Smiddy', 19, 99, 80),
(216, '110', 'Brian Otridge', 24, 0, NULL);

