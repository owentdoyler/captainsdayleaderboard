<?php
	//class which hold all the players information 
	class Player{
		public $name = 'no name specified';
		public $score18Net = -1;
		public $netTotal = 0;
		public $grossTotal = 0;
		public $id = -1;
		public $handicap = -1000;
		public $adjustedHandicap = -1000;
		public $teeTime;
		
		public $score18 = -1;
		public $score1 = -1;
		public $score2 = -1;
		public $score3 = -1;
		public $score4 = -1;
		public $score5 = -1;
		public $score6 = -1;
		public $score7 = -1;
		public $score8 = -1;
		public $score9 = -1;
		
		public function score18Net(){
			return ($this->score18 - $this->handicap);
		}
		
		public function scoreOrTeeTime(){
			if($this->score18Net){
				return $this->score18;
			}
			else{
				$time = substr($this->teeTime, 0, 5);
				return $time;
			}
		}
		
		public function thru(){
			$thru = 0;
			$thru += ($this->score1 <= 0)? 0 : 1;
			$thru += ($this->score2 <= 0)? 0 : 1;
			$thru += ($this->score3 <= 0)? 0 : 1;
			$thru += ($this->score4 <= 0)? 0 : 1;
			$thru += ($this->score5 <= 0)? 0 : 1;
			$thru += ($this->score6 <= 0)? 0 : 1;
			$thru += ($this->score7 <= 0)? 0 : 1;
			$thru += ($this->score8 <= 0)? 0 : 1;
			$thru += ($this->score9 <= 0)? 0 : 1;

			if($thru < 9){
				return $thru;
			}
			else{
				return 'F';
			}
		}
	}
?>