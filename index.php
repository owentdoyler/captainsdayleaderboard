<?php
require_once('query.php');
$page = $_SERVER['PHP_SELF'];
$sec = "20";
?>
<!DOCTYPE html>
<html>
    <head>

		<meta http-equiv="refresh" content="<?php echo $sec?>;URL='<?php echo $page?>'">
        <link rel="stylesheet" type="text/css" href="style.css" />
        <title>Standings</title>
    </head>
    <body id="playoff">
        <div id="banner">
				<img src="lutt_logo_green.png" id="logo">
				<h2 id="heading">Dave Heary's Captains Prize<br>Playoff Leaderboard</h2>
        </div>
        <div id="table">
            <table id="standings">
                <tr>
                    <td><h5>POS</h5></td>
                    <td id="nameCol"><h5>PLAYER	(H'CAP)</h5></td>
					<td><h5>18 HOLE<br>NET<h5></td>
					<td><h5 style="font-size: 10px;">ADJUSTED<br>H'CAP</h5></td>
                    <td><h5>NET<br>TOTAL</h5></td>
					<td><h5>THRU</h5></td>
                </tr>
            <?php
				$position = 1;
				//loop throug all the players and display the standings in a table
				foreach($standings as $player){
					$thru = $player->thru();

					echo "<tr>";
						echo "<td><p class=\"leaderboardData\">$position</p></td>";
						$position = $position + 1;
						echo "<td><p class=\"name\">$player->name <span style=\"color:#8d8d8d;\">($player->handicap)</span></p></td>";

						echo "<td><p class=\"leaderboardData\">";
						echo $player->score18Net();
						echo "</p></td>";

						echo "<td><p class=\"leaderboardData\">$player->adjustedHandicap</p></td>";
						if($thru === 'F'){
							echo "<td  id=\"totalScore\" style=\"background-color:#696969;\"><p class=\"leaderboardData\">$player->netTotal</p></td>";
						}
						else{
							echo "<td id=\"totalScore\" style=\"background-color:red;\"><p class=\"leaderboardData\">$player->netTotal</p></td>";
						}

						echo "<td><p class=\"leaderboardData\">";
						echo $player->thru();
						echo "</p></td>";
					echo "</tr>";
				}
			?>
            </table>
			<?php
				if(count($standings) < 1){
					echo "<h2 style=\"text-align: center; color: red;\">The Playoff will begin at 17:00</h2>";
				}
			?>

			<div class="navigation">
				<a href="18hole_standings.php">18 Hole Leaderboard</a>
			</div>
    </body>
</html>
