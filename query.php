<?php
	
	//creates an array of all the player objects
	require("connect.php");
	require("player.php");
	
	$scores_query = "SELECT * FROM playoff_scores ORDER BY netTotal, countback, playoffTotalNett";
	
	$standings = array();
	$response = @mysqli_query($database, $scores_query);
	
	if($response){
		while($row = mysqli_fetch_array($response)){
			
			$player = new Player();
			$player->name = $row['name'];
			$player->netTotal = $row['netTotal'];
			$player->id = $row['id'];
			$player->handicap = $row['handicap'];
			$player->adjustedHandicap = $row['handicapAdjust'];
			$player->grossTotal = $row['grossTotal'];
			$player->score18 = $row['score18'];
			
			if($row['score1']){
				$player->score1 = $row['score1']; 
			}
			
			if($row['score2']){
				$player->score2 = $row['score2']; 
			}
			
			if($row['score3']){
				$player->score3 = $row['score3']; 
			}
			
			if($row['score4']){
				$player->score4 = $row['score4']; 
			}
			
			if($row['score5']){
				$player->score5 = $row['score5']; 
			}
			
			if($row['score6']){
				$player->score6 = $row['score6']; 
			}

			if($row['score7']){
				$player->score7 = $row['score7']; 
			}

			if($row['score8']){
				$player->score8 = $row['score8']; 
			}
			
			if($row['score9']){
				$player->score9 = $row['score9']; 
			}
			array_push($standings, $player);
		}
	}
	
?>