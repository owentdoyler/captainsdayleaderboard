<?php

	//creates an array of all the player objects
	require("connect.php");
	require("player.php");

	$scores_query = "SELECT * FROM 18hole_scores ORDER BY STR_TO_DATE(teeTime, '%H:%i:%s')";

	$standings = array();
	$response = @mysqli_query($database, $scores_query);

	if($response){
		while($row = mysqli_fetch_array($response)){

			$player = new Player();
			$player->name = $row['name'];
			$player->id = $row['id'];
			$player->handicap = $row['handicap'];
			$player->score18 = $row['score18'];
			$player->teeTime = $row['teeTime'];
			$player->score18Net = $row['score18Nett'];

			array_push($standings, $player);
		}
	}

?>
